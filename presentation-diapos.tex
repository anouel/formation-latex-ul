%%% Copyright (C) 2018 Vincent Goulet
%%%
%%% Ce fichier fait partie du projet
%%% «Rédaction avec LaTeX»
%%% https://gitlab.com/vigou3/formation-latex-ul
%%%
%%% Cette création est mise à disposition sous licence
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\section{Présentation de {\TeX} et {\LaTeX}}

\subsection{Description sommaire}

\begin{frame}
  \frametitle{Ce que c'est}
  \begin{columns}
    \begin{column}{.5\textwidth}
      \begin{itemize}
      \item {\TeX} est un système de mise en page (\emph{typesetting})
        ou de préparation de documents
      \item {\LaTeX} est un ensemble de macro-commandes pour faciliter
        l'utilisation de {\TeX}
      \item Langage de balisage (\emph{Markup Language}) pour indiquer
        la mise en forme du texte
      \item Accent mis sur la production de documents de grande
        qualité à la typographie soignée (surtout pour les
        mathématiques)
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \centering
      \includegraphics[width=\linewidth]{images/knuth} \\
      \footnotesize Donald Knuth, créateur de \TeX
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Ce que ce n'est pas}
  \begin{tabular}{lcl}
    Un traitement de texte & \faArrowRight & priorité accordée
                                             à la qualité de
                                             la mise en page \\[6pt]
    WYSIWYG & \faArrowRight & plutôt What You See Is What
                              You \emph{Mean} \\[6pt]
    Incompatible & \faArrowRight & format identique sur tous
                                   les systèmes d'exploitation \\[6pt]
    Instable & \faArrowRight & noyau arrivé à maturité \\[6pt]
    Imprévisible & \faArrowRight & {\LaTeX} fait ce qu'on
                                   lui demande, ni plus, ni moins
  \end{tabular}
\end{frame}

\begin{frame}
  \frametitle{Quelques choses simples à réaliser avec {\LaTeX}}

  \begin{itemize}
  \item Page de titre
  \item Table des matières
  \item Numérotation des pages
  \item Figures et tableaux: disposition, numérotation, renvois
  \item Équations mathématiques: disposition, numérotation, renvois
  \item Citations et composition de la bibliographie
  \item Coupure de mots
  \item Document recto verso
  \end{itemize}
\end{frame}

\subsection{Processus de création}

\begin{frame}
  \frametitle{Processus de création d'un document {\LaTeX}}
  \Huge
  \begin{minipage}[t]{0.6\linewidth}
    \begin{minipage}[t]{0.4\textwidth}
      \centering
      \faFileTextO \\ \bigskip
      \footnotesize
      rédaction du texte et balisage avec un \emph{éditeur de texte}
    \end{minipage}
    \hfill\faArrowRight\hfill
    \begin{minipage}[t]{0.4\textwidth}
      \centering
      \faCogs \\  \bigskip
      \footnotesize
      compilation avec un \emph{moteur} {\TeX} depuis la ligne de commande
    \end{minipage} \\ \bigskip

    \onslide<2>{%
      \color{alert}
      \rule[6pt]{\linewidth}{2pt} \\
      \footnotesize\centering
      facilité par l'utilisation d'un \\
      logiciel intégré de rédaction \\
      (Texmaker, TeXShop, Emacs, \dots)
    }
  \end{minipage}
  \begin{minipage}[t]{0.35\linewidth}
    \hfill\faArrowRight\hfill
    \begin{minipage}[t]{0.7\textwidth}
      \centering
      \faFilePdfO \\  \bigskip
      \footnotesize
      visualisation avec visionneuse externe (Aperçu,
      SumatraPDF, etc.)
    \end{minipage}
  \end{minipage}
\end{frame}

\subsection{[~Exercice~]}

\begin{exercice}
  Démarrer le logiciel \alert{Texmaker} (Windows), \alert{TeXShop}
  (macOS) ou tout autre éditeur ou logiciel intégré de rédaction de
  votre choix.

  \begin{enumerate}
  \item Ouvrir et compiler le fichier \fichier{exercice\_minimal.tex}.
  \item Ajouter du texte en français (avec accents) et observer le
    résultat.
  \end{enumerate}
\end{exercice}

\subsection{Outils de production}

\begin{frame}
  \frametitle{Moteurs et formats}
  \centering

  \begin{tabular}{r}
    \\ \addlinespace[8pt] \\ \\
    \color{alert}\faArrowRight \\
    \color{alert}\faArrowRight
  \end{tabular}
  \hspace{-4mm}
  \begin{tabularx}{0.7\linewidth}{Xlc}
    \toprule[2pt]
    Moteur & Format & Fichier de sortie \\
    \midrule
    \texttt{tex} & plain \TeX & DVI \\
    \texttt{tex} (\texttt{latex}) & \LaTeX & DVI \\
    \texttt{pdftex} (\texttt{pdflatex}) & pdf\LaTeX & PDF \\
    \texttt{xetex} (\texttt{xelatex}) & \XeLaTeX & PDF \\
    \bottomrule[2pt]
  \end{tabularx}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Faits amusants}
  \begin{itemize}
  \item {\TeX} est aujourd'hui considéré exempt de bogue
  \item Récompense si vous en trouvez un!
  \item Numéro de version de {\TeX} converge vers $\pi$:
\begin{lstlisting}
$ tex --version
TeX `\textbf{3.14159265}' (TeX Live 2018)
kpathsea version 6.3.0
Copyright 2018 D.E. Knuth.
[...]
\end{lstlisting} %$
  \end{itemize}
\end{frame}

\subsection{[~Exercice~]}

\begin{exercice}
  Question de voir ce que {\LaTeX} peut faire, compiler le document
  élaboré \fichier{exercice\_demo.tex} de la manière suivante:
  \begin{enumerate}[i)]
  \item une fois avec \texttt{LaTeX};
  \item une fois avec \texttt{BibTeX};
  \item deux à trois fois avec \texttt{LaTeX}.
  \end{enumerate}
\end{exercice}

%%% Local Variables:
%%% TeX-master: "formation-latex-ul-diapos"
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
